﻿// Comando 1
SELECT * FROM Customers 
WHERE customerID IN  
    (
        SELECT CustomerId FROM 
        (
            SELECT O.CustomerID, SUM(OD.UnitPrice * (1 - OD.Discount) * OD.Quantity) as Total 
            FROM [Order Details] OD, Products P, Orders O
            WHERE p.ProductID = OD.ProductID AND O.OrderID = OD.OrderID AND p.CategoryID = 1
            GROUP BY O.CustomerID 
        ) AS OrdersFromCategory
        WHERE Total >= 10000
    )
    
    
// Comando 2
SELECT * FROM customers 
WHERE customerID IN 
    (
        SELECT CustomerId FROM 
        (
            SELECT OD.OrderID, O.CustomerID, SUM(OD.UnitPrice * (1 - OD.Discount) * OD.Quantity) as Total 
            FROM [Order Details] OD, Products P, Orders O
            WHERE p.ProductID = OD.ProductID And O.OrderID = OD.OrderID and p.CategoryID = 1
            GROUP BY OD.OrderID, O.CustomerID 
        ) AS OrdersFromCategory
        WHERE Total >= 10000
    )
