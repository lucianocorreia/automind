﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Automind.Models;
using System.Text;

namespace Automind.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["list"] = encomendas;
            return View(new EncomendaViewModel());
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        private static readonly List<Encomenda> encomendas = new List<Encomenda>();


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult InserirEncomenda(EncomendaViewModel encomendaViewModel)
        {
            if(Request.Form["Calcular"] == "Calcular")
            {
                ModelState.Clear();
                return CaulculaValores(encomendaViewModel.VolumeCargaCaminhao);
            }

            if (ModelState.IsValid)
            {
                if (!encomendas.Exists(e => e.Id == encomendaViewModel.Id))
                {
                    encomendas.Add(new Encomenda
                    {
                        Id = encomendaViewModel.Id,
                        Volume = encomendaViewModel.Volume,
                        ValorFrete = encomendaViewModel.ValorFrete
                    });
                    //encomendaViewModel.Clear();
                }
                else ModelState.AddModelError(string.Empty, "Encomenda ja cadastrada");

                ViewData["list"] = encomendas;
                return RedirectToAction("index", new EncomendaViewModel());
            }

            ViewData["list"] = encomendas;
            return View("index", new EncomendaViewModel());
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
                if (encomendas.Exists(e => e.Id == id))
                {
                    encomendas.Remove(encomendas.FirstOrDefault(e => e.Id == id));
                }

                ViewData["list"] = encomendas;
                return RedirectToAction("index", new EncomendaViewModel());
            }

            ViewData["list"] = encomendas;
            return View("index", new EncomendaViewModel());
        }


        public IActionResult CaulculaValores(decimal volumeCargaCaminhao)
        {
            // Lista para armazenar os itens e as quantidades e valor de frete
            List<Tuple<Encomenda, decimal, int>> resultado = new List<Tuple<Encomenda, decimal, int>>();

            foreach (var encomenda in encomendas)
            {
                var quantidadeCaminhao = Convert.ToInt32(Math.Truncate(volumeCargaCaminhao / encomenda.Volume));
                var precoTotalFrete = quantidadeCaminhao * encomenda.ValorFrete;

                resultado.Add(new Tuple<Encomenda, decimal, int>(encomenda, precoTotalFrete, quantidadeCaminhao));
            }

            // orderna pelo maior valor do Frete
            var resultadoOrdenado = resultado.OrderByDescending(e => e.Item2);

            StringBuilder resposta = new StringBuilder();

            decimal volumeDisponivelCaminhao = volumeCargaCaminhao;
            decimal valorFinalFrete = decimal.Zero;

            foreach (var item in resultadoOrdenado)
            {
                var quantidadeItensNoCaminhao = 
                    Convert.ToInt32(Math.Truncate(volumeDisponivelCaminhao / item.Item1.Volume));
                volumeDisponivelCaminhao = 
                    volumeDisponivelCaminhao - (quantidadeItensNoCaminhao * item.Item1.Volume);

                valorFinalFrete += quantidadeItensNoCaminhao * item.Item1.ValorFrete;

                resposta.Append(
                    string.Format("Item id: <b>{0}</b>, Quantidade: <b>{1}</b>, Frete: <b>{2}</b> <br/>", 
                                  item.Item1.Id, 
                                  quantidadeItensNoCaminhao, 
                                  (quantidadeItensNoCaminhao * item.Item1.ValorFrete).ToString("0.00") ) );
            }

            resposta.Append(string.Format("</br>----------------</br>Valor total do Frete: <b>{0}</b>", valorFinalFrete.ToString("0.00")));


            var encomendasViewModel = new EncomendaViewModel();
            encomendasViewModel.VolumeCargaCaminhao = volumeCargaCaminhao;
            encomendasViewModel.Resposta = resposta.ToString();
            ViewData["list"] = encomendas;
            return View("index", encomendasViewModel);

        }


    }
}
