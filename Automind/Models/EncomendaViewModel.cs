﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Automind.Models
{
    public class EncomendaViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [Range(0.00001, Double.MaxValue, ErrorMessage = "Informe o Volume da encomenda")]
        public decimal Volume { get; set; }
        [Required]
        [Range(0.00001, Double.MaxValue, ErrorMessage = "Informe o Valor do Frete")]
        public decimal ValorFrete { get; set; }

        //[Range(0.00001, Double.MaxValue, ErrorMessage = "Informe o Volume de carga do caminhão")]
        public decimal VolumeCargaCaminhao { get; set; }

        public string Resposta { get; set; }

    }
}
